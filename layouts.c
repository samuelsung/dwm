void grid(Monitor *m);
void horizontalgrid(Monitor *m);
void mastergrid(Monitor *m);
void horizontalmastergrid(Monitor *m);
void gaplessgrid(Monitor *m, Bool isVertical);
void mastergaplessgrid(Monitor *m, Bool isVertical);

void
grid(Monitor *m) {
  gaplessgrid(m, True);
}

void
horizontalgrid(Monitor *m) {
  gaplessgrid(m, False);
}

void
mastergrid(Monitor *m) {
  mastergaplessgrid(m, True);
}

void
horizontalmastergrid(Monitor *m) {
  mastergaplessgrid(m, False);
}

void
gaplessgrid(Monitor *m, Bool isVertical) {
	unsigned int n, cols, rows, cn, rn, i, cx, cy, cw, ch;
	Client *c;

	for(n = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), n++) ;
	if(n == 0)
		return;

	/* grid dimensions */
	for(cols = 0; cols <= n/2; cols++)
		if(cols*cols >= n)
			break;
	if(n == 5) /* set layout against the general calculation: not 1:2:2, but 2:3 */
		cols = 2;
	rows = n/cols;

	/* window geometries */
  if (isVertical)
    cw = cols ? ((m->ww - m->gappx) / cols) - m->gappx : m->ww - 2 * m->gappx;
  else
    ch = cols ? ((m->wh - m->gappx) / cols) - m->gappx : m->wh - 2 * m->gappx;
	cn = 0; /* current column number */
	rn = 0; /* current row number */
	for(i = 0, c = nexttiled(m->clients); c; i++, c = nexttiled(c->next)) {
		if(i/rows + 1 > cols - n%cols)
			rows = n/cols + 1;
    if (isVertical) {
      ch = rows ? ((m->wh - m->gappx) / rows) - m->gappx : m->wh - 2 * m->gappx;
      cx = m->wx + m->gappx + cn*(cw + m->gappx);
      cy = m->wy + m->gappx + rn*(ch + m->gappx);
    } else {
      cw = rows ? ((m->ww - m->gappx) / rows) - m->gappx : m->ww - 2 * m->gappx;
      cx = m->wx + m->gappx + rn*(cw + m->gappx);
      cy = m->wy + m->gappx + cn*(ch + m->gappx);
    }
		resize(c, cx, cy, cw - 2 * c->bw, ch - 2 * c->bw, False);
		rn++;
		if(rn >= rows) {
			rn = 0;
			cn++;
		}
	}
}


void
mastergaplessgrid(Monitor *m, Bool isVertical)
{
	unsigned int i, n, mn, tn, mw, mcols, mrows, tcols, trows, cn, rn, cw, ch, cx, cy;
	Client *c;

	for (n = 0, c = nexttiled(m->clients); c; c = nexttiled(c->next), n++);
	if (n > m->nmaster) {
    mn = m->nmaster;
    tn = n - m->nmaster;
		mw = m->nmaster ? m->ww * m->mfact : 0;
  } else {
    mn = n;
    tn = 0;
		mw = m->ww;
  }

  if (mn == 0 || tn == 0) {
    gaplessgrid(m, isVertical);
    return;
  }

	for(mcols = 0; mcols <= mn/2; mcols++)
		if(mcols*mcols >= mn)
			break;
	if(mn == 5) /* set layout against the general calculation: not 1:2:2, but 2:3 */
		mcols = 2;
	mrows = mn/mcols;

	for(tcols = 0; tcols <= n/2; tcols++)
		if(tcols*tcols >= tn)
			break;
	if(tn == 5) /* set layout against the general calculation: not 1:2:2, but 2:3 */
		tcols = 2;
	trows = tn/tcols;

  /* set layout for master windows */
  if (isVertical)
    cw = mcols ? (mw / mcols) - m->gappx : mw - m->gappx;
  else
    ch = mcols ? ((m->wh - m->gappx) / mcols) - m->gappx : m->wh - 2 * m->gappx;
  cn = 0;
  rn = 0;
	for (i = 0, c = nexttiled(m->clients); c && i < mn; c = nexttiled(c->next), i++) {
    if(i/mrows + 1 > mcols - mn%mcols)
      mrows = mn/mcols + 1;
    if (isVertical) {
      ch = mrows ? ((m->wh - m->gappx) / mrows) - m->gappx : m->wh - 2 * m->gappx;
      cx = m->wx + m->gappx + cn * (cw + m->gappx);
      cy = m->wy + m->gappx + rn * (ch + m->gappx);
    } else {
      cw = mrows ? (mw / mrows) - m->gappx : mw - m->gappx;
      cx = m->wx + m->gappx + rn * (cw + m->gappx);
      cy = m->wy + m->gappx + cn * (ch + m->gappx);
    }
    resize(c, cx, cy, cw - 2 * c->bw, ch - 2 * c->bw, False);
    rn++;
    if(rn >= mrows) {
      rn = 0;
      cn++;
    }
  }

  /* set layout for tile windows */
  if (isVertical)
    cw = tcols ? (m->ww - mw - m->gappx) / tcols - m->gappx : m->ww - mw - 2 * m->gappx;
  else
    ch = tcols ? (m->wh - m->gappx) / tcols - m->gappx : m->wh - 2 * m->gappx;
  cn = 0;
  rn = 0;
	for (i = 0; c && i < tn; c = nexttiled(c->next), i++) {
    if(i/trows + 1 > tcols - tn%tcols)
      trows = tn/tcols + 1;
    if (isVertical) {
      ch = trows ? ((m->wh - m->gappx) / trows) - m->gappx : m->wh - 2 * m->gappx;
      cx = m->wx + mw + m->gappx + cn * (cw + m->gappx);
      cy = m->wy + m->gappx + rn * (ch + m->gappx);
    } else {
      cw = trows ? ((m->ww - mw - m->gappx) / trows) - m->gappx : m->ww - mw - 2 * m->gappx;
      cx = m->wx + mw + m->gappx + rn * (cw + m->gappx);
      cy = m->wy + m->gappx + cn * (ch + m->gappx);
    }
    resize(c, cx, cy, cw - 2 * c->bw, ch - 2 * c->bw, False);
    rn++;
    if(rn >= trows) {
      rn = 0;
      cn++;
    }
  }
}
