/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx       = 2;        /* border pixel of windows */
static const unsigned int gappx          = 6;        /* gaps between windows */
static const unsigned int snap           = 32;       /* snap pixel */
static const int showbar                 = 1;        /* 0 means no bar */
static const int topbar                  = 1;        /* 0 means bottom bar */
static const int vertpad                 = 6;       /* vertical padding of bar */
static const int sidepad                 = 6;       /* horizontal padding of bar */
static const int horizpadbar             = 0;        /* horizontal padding for statusbar */
static const int vertpadbar              = 2;        /* vertical padding for statusbar */
static const unsigned int systraypinning = 1;   /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayspacing = 2;   /* systray spacing */
static const int systraypinningfailfirst = 1;   /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray             = 1;   /* 0 means no systray */
static const int focusonwheel            = 1;
static const int isAttachaside           = 0;
static const char *fonts[]               = { "Fira Code Medium:size=10:antialias=true:autohint=true:style=Medium", "Noto Color Emoji:size=10:style=Regular" };
static const char dmenufont[]            = { "Fira Code Medium:size=10:antialias=true:autohint=true:style=Medium" };
static char normbgcolor[]                = "#2e3440";  /*Nord 0*/
static char normbordercolor[]            = "#2e3440";  /*Nord 0*/
static char normfgcolor[]                = "#d8dee9";  /*Nord 4*/
static char selfgcolor[]                 = "#3b4252";  /*Nord 1*/
static char selbordercolor[]             = "#81a1c1";  /*Nord 9*/
static char selbgcolor[]                 = "#81a1c1";  /*Nord 9*/
static char *colors[][3] = {
  /*               fg           bg           border   */
  [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
  [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};
static const unsigned int baralpha = 0xd0;
static const unsigned int borderalpha = OPAQUE;
static const unsigned int alphas[][3]      = {
	/*               fg      bg        border     */
	[SchemeNorm] = { OPAQUE, baralpha, borderalpha },
	[SchemeSel]  = { OPAQUE, baralpha, borderalpha },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

#include "layouts.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
  { "VVV",      grid },
  { "HHH",      horizontalgrid },
  { "masterVVV", mastergrid },
  { "masterHHH", horizontalmastergrid },
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { "st", NULL };

static Key keys[] = {
	/* modifier                     key        function        argument */
	// { MODKEY,                       XK_p,      spawn,          {.v = dmenucmd } },
	// { MODKEY|ShiftMask,             XK_Return, spawn,          {.v = termcmd } },
	// { MODKEY,                       XK_b,      togglebar,      {0} },
	// { MODKEY|ShiftMask,             XK_j,      rotatestack,    {.i = +1 } },
	// { MODKEY|ShiftMask,             XK_k,      rotatestack,    {.i = -1 } },
	// { MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	// { MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	// { MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	// { MODKEY,                       XK_d,      incnmaster,     {.i = -1 } },
	// { MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	// { MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	// { MODKEY,                       XK_Return, zoom,           {0} },
	// { MODKEY,                       XK_Tab,    view,           {0} },
	// { MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	// { MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	// { MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	// { MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	// { MODKEY,                       XK_g,      setlayout,      {.v = &layouts[3]} },
	// { MODKEY,                       XK_space,  setlayout,      {0} },
	// { MODKEY|ControlMask,		        XK_comma,  cyclelayout,    {.i = -1 } },
	// { MODKEY|ControlMask,           XK_period, cyclelayout,    {.i = +1 } },
	// { MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	// { MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },
	// { MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	// { MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	// { MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	// { MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	// { MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	// { MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	// { MODKEY,                       XK_Left,   viewtoleft,     {0} },
	// { MODKEY,                       XK_Right,  viewtoright,    {0} },
	// { MODKEY|ShiftMask,             XK_Left,   tagtoleft,      {0} },
	// { MODKEY|ShiftMask,             XK_Right,  tagtoright,     {0} },
	// { MODKEY,                       XK_minus,  setgaps,        {.i = -1 } },
	// { MODKEY,                       XK_equal,  setgaps,        {.i = +1 } },
	// { MODKEY|ShiftMask,             XK_equal,  setgaps,        {.i = 0  } },
	// { MODKEY,                       XK_F5,     xrdb,           {.v = NULL } },
	// TAGKEYS(                        XK_1,                      0)
	// TAGKEYS(                        XK_2,                      1)
	// TAGKEYS(                        XK_3,                      2)
	// TAGKEYS(                        XK_4,                      3)
	// TAGKEYS(                        XK_5,                      4)
	// TAGKEYS(                        XK_6,                      5)
	// TAGKEYS(                        XK_7,                      6)
	// TAGKEYS(                        XK_8,                      7)
	// TAGKEYS(                        XK_9,                      8)
	// { MODKEY|ShiftMask,             XK_q,      quit,           {0} },
	{ MODKEY|ControlMask|ShiftMask, XK_q,      quit,           {1} }, 
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

void
viewpre(const Arg *arg)
{
  view(&((Arg) {}));
}
 
void
setlayoutex(const Arg *arg)
{
	setlayout(&((Arg) { .v = &layouts[arg->i] }));
}

void
viewex(const Arg *arg)
{
	view(&((Arg) { .ui = 1 << arg->ui }));
}

void
viewall(const Arg *arg)
{
	view(&((Arg){.ui = ~0}));
}

void
tagmonandfocus(const Arg *arg)
{
  tagmon(&((Arg) { .i = arg->i}));
  focusmon(&((Arg) { .i = arg->i}));
}

void
tagtoleftandfocus(const Arg *arg)
{
  tagtoleft(&((Arg) {}));
  viewtoleft(&((Arg) {}));
}

void
tagtorightandfocus(const Arg *arg)
{
  tagtoright(&((Arg) {}));
  viewtoright(&((Arg) {}));
}

void
toggleviewex(const Arg *arg)
{
	toggleview(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagex(const Arg *arg)
{
	tag(&((Arg) { .ui = 1 << arg->ui }));
}

void
toggletagex(const Arg *arg)
{
	toggletag(&((Arg) { .ui = 1 << arg->ui }));
}

void
tagall(const Arg *arg)
{
	tag(&((Arg){.ui = ~0}));
}

/* signal definitions */
/* signum must be greater than 0 */
/* trigger signals using `xsetroot -name "fsignal:<signame> [<type> <value>]"` */
static Signal signals[] = {
	/* signum           function */
	{ "cyclelayout",     cyclelayout },
	{ "focusstack",     focusstack },
	{ "rotatestack",     rotatestack },
	{ "setmfact",       setmfact },
	{ "togglebar",      togglebar },
	{ "incnmaster",     incnmaster },
	{ "togglefloating", togglefloating },
	{ "togglefullscr", togglefullscr },
	{ "toggletile", toggletile },
	{ "focusmon",       focusmon },
	{ "tagmonandfocus",       tagmonandfocus },
	{ "tagmon",         tagmon },
	{ "viewtoleft",     viewtoleft },
	{ "viewtoright",     viewtoright },
	{ "tagtoleft",      tagtoleft },
	{ "tagtoright",      tagtoright },
	{ "tagtoleftandfocus",      tagtoleftandfocus },
	{ "tagtorightandfocus",      tagtorightandfocus },
	{ "zoom",           zoom },
	{ "view",           view },
	{ "viewall",        viewall },
	{ "viewex",         viewex },
	{ "viewpre",         viewpre },
	{ "toggleview",     view },
	{ "toggleviewex",   toggleviewex },
	{ "tag",            tag },
	{ "tagall",         tagall },
	{ "tagex",          tagex },
	{ "toggletag",      tag },
	{ "toggletagex",    toggletagex },
	{ "killclient",     killclient },
	{ "quit",           quit },
	{ "setlayout",      setlayout },
	{ "setlayoutex",    setlayoutex },
	{ "setgaps",    setgaps },
};
